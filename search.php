<?php

get_header();
?>

<div id="fh5co-main">
    <div class="container">

        <?php if (have_posts()): ?>



        <div class="row">

            <div id="fh5co-board" data-columns>


                    <?php while (have_posts()):the_post(); ?>

                        <div class="item">
                            <div class="item-box">
                                <a href="<?php the_permalink(); ?>"><img
                                            src="<?php echo get_the_post_thumbnail_url(); ?>"
                                            alt="">
                                    <div class="item-blur">
                                        <h4><?php the_title(); ?></h4>
                                    </div>
                                </a>

                            </div>
                        </div>

                    <?php endwhile; ?>


            </div>
        </div>


        <?php else: ?>

            <div class="not-search">

                <h6>Nothing matched your search. You could try again with some different keywords.</h6>

                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?> "><button class="btn btn-warning">Go back</button></a>




            </div>
        <?php endif; ?>



    </div>
</div>

<?php
get_footer();
?>
