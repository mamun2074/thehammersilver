
<?php get_header(); ?>
<!--Blog main area-->

<div class="stories-area">
    <div class="container">

        <div class="page-heading">

                <h2>

                    <?php

                         $page_title=get_the_title(); 
                         $home="Home";
                  
                        if($page_title==$home){

                        }else{
                          echo $page_title;
                        }


                     ?>

                </h2>

        </div>

        <div class="row">
            <div class="col-md-12">

                <?php while(have_posts()):the_post(); ?>

                    <?php the_content(); ?>

                <?php endwhile; ?>
                
                

            </div>
        </div>


    </div>
</div>

<?php get_footer(); ?>