<?php get_header(); ?>
    <!--Blog main area-->

    <div id="fh5co-main">
        <div class="container">

            <?php

            get_search_form();

            ?>

            <div class="row">

                <div id="fh5co-board" data-columns>


                    <?php while (have_posts()):the_post(); ?>

                        <div class="item">
                            <div class="item-box">
                                <a href="<?php the_permalink(); ?>"><img
                                            src="<?php echo get_the_post_thumbnail_url(); ?>"
                                            alt="">
                                    <div class="item-blur">
                                        <h4><?php the_title(); ?></h4>
                                    </div>
                                </a>

                            </div>
                        </div>

                    <?php endwhile; ?>


                </div>


            </div>

            <div class="paginaiton">

<!--                <ul>-->
<!--                    <li class="active"><a href="#">1</a></li>-->
<!--                    <li><a href="#">2</a></li>-->
<!--                    <li><a href="#">3</a></li>-->
<!--                    <li><a href="">Next</a></li>-->
<!--                </ul>-->



                <?php

                the_posts_pagination( array(
                    'mid_size'  => 2,
                    'prev_text' => __( 'Prev', 'textdomain' ),
                    'next_text' => __( 'Next', 'textdomain' ),
                ) );

                ?>


            </div>

        </div>
    </div>


<?php get_footer(); ?>