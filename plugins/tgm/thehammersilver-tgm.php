<?php

require_once('class-tgm-plugin-activation.php');


add_action('tgmpa_register', 'thehammersilverPluginRequired');


function thehammersilverPluginRequired()
{


    $plugins = array(

//        visual composer
        array(
            'name' => 'Visual composer',
            'slug' => 'js_composer',
            'source' => get_template_directory_uri() . '/plugins/tgm/plugins/js_composer.zip',
            'required' => true,

        ),

//        Revulotions slider
        array(
            'name' => 'Revulotion slider',
            'slug' => 'revslider',
            'source' => get_template_directory_uri() . '/plugins/tgm/plugins/revslider.zip',
            'required' => true,

        ),

//        Woocommerce plugin
        array(
            'name' => 'WooCommerce',
            'slug' => 'woocommerce',
            'required' => true,

        ),

        array(
            'name' => 'WP Subscribe',
            'slug' => 'wp-subscribe',
            'required' => true,
        ),

        array(
            'name' => 'Instagram Feed',
            'slug' => 'instagram-feed',
            'required' => false,
        ),

//        contact form 7

        array(

            'name'  =>'Contact form 7',
            'slug'  =>'contact-form-7',
            'required'=>false,

        ),

//        Responsive menu


        array(

            'name'  =>'Responsive Menu',
            'slug'  =>'responsive-menu',
            'required'=>true,

        ),

    );


    $config = array(
        'id' => 'thehammersilver',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug' => 'themes.php',            // Parent menu slug.
        'capability' => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices' => true,                    // Show admin notices or not.
        'dismissable' => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message' => '',                      // Message to output right before the plugins table.

    );


    tgmpa($plugins, $config);

}