<?php
/*
 * Template Name:music
 */


get_header(); ?>



<!--music header start-->
<div class="tgf-music-area">
    <div class="container">
        <div class="page-heading">
            <h2>Tgf Music</h2>
        </div>


        <div class="tgf-music-blog">
            <div class="row">

                <?php

                $music_post_type = new WP_Query(array(
                    'post_type' => 'music',
                    'posts_per_page'=>8,
                ));


                while ($music_post_type->have_posts()):$music_post_type->the_post();
                    ?>
                    <div class="col-md-3 col-sm-6">
                        <div class="tgf-content">
                            <h3><?php the_title(); ?></h3>
                            <a href="<?php the_permalink(); ?>"><?php
                                the_post_thumbnail(); ?></a>
                        </div>
                    </div>

                <?php endwhile; ?>


            </div>

            <div class="paginaiton">

<!--                <ul>-->
<!--                    <li class="active"><a href="#">1</a></li>-->
<!--                    <li><a href="#">2</a></li>-->
<!--                    <li><a href="#">3</a></li>-->
<!--                    <li><a href="">Next</a></li>-->
<!--                </ul>-->

            </div>


        </div>
    </div>
</div>

<div class="tgm-instragram-area">
    <div class="container">
        <div class="header">
            <h4><span>TGF MUSIC ON INSTAGRAM</span></h4>

        </div>
        <div class="row">


            <?php echo do_shortcode('[instagram-feed]'); ?>


        </div>

        <!--        <div class="instragram-button">-->
        <!--            <a href="#"><button type="button" class="btn btn-default">Load More</button></a>-->
        <!--            <a href="#"><button type="button" class="btn btn-default">Follow us</button></a>-->
    </div>

</div>



<?php get_footer(); ?>




