<?php get_header(); ?>

<!--Main Menu area End-->


<div class="single-body-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php while (have_posts()):the_post() ?>
                
                <div class="single-body">
                    <h2><?php the_title(); ?></h2>

                    <h5><?php the_time('M  d, Y'); ?> </h5>
                    <?php the_content(); ?>

                    <img src="<?php echo the_post_thumbnail_url(); ?>" alt="">

                    <p><?php the_title(); ?></p>

                </div>
            <?php endwhile; ?>


            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>