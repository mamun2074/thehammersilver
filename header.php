<!doctype html>
<html <?php language_attributes(); ?> >
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">


    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?> >

<?php global $redux_demo;

//
//echo "<pre>";
//
//print_r($redux_demo['instagram-link']);
//
//die();

?>

<?php global $woocommerce; ?>

<!--Top menu area start-->
<div class="menu-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 res">
                <div class="social-area pull-left">
                    <ul>
                        <li><a target="_blank" href="<?php echo $redux_demo['instagram-link']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a target="_blank" href="<?php echo $redux_demo['twitter-link']; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a target="_blank" href="<?php echo $redux_demo['fb-link']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 res">
                <div class="accaunt-area pull-right">
                    <ul>
                        <li>
<!--                            <a href="#">-->
<!--                                <input type="text">-->
<!--                                <i class="fa fa-search" aria-hidden="true"></i>-->
<!--                            </a>-->

                            <?php

                            get_product_search_form();

                            ?>
                        </li>
                        <li>

                            <?php if ( is_user_logged_in() ) { ?>
                                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a>
                            <?php }
                            else { ?>
                                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><?php _e('Login / Register','woothemes'); ?></a>
                            <?php } ?>

                        </li>
                        <li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="fa fa-shopping-cart"
                                           aria-hidden="true"></i> <?php
                                echo $woocommerce->cart->total; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Top menu area end-->

<!-- Hide menu -->

<div class="hide-menu-top">
    <div class="container">
        <div class="hide-menu">
            <div class="hide-menu-left">

                <a href="#"><i class=""></i></a>

            </div>

            <div class="hide-menu-right">



                <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="fa fa-shopping-cart"></i><?php echo $woocommerce->cart->total; ?></a>
                <?php if ( is_user_logged_in() ) { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a>
                <?php }
                else { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><?php _e('Login / Register','woothemes'); ?></a>
                <?php } ?>

            </div>
        </div>
    </div>

</div>



<!--Main Menu area Start -->

<div class="main-menu-area">
    <div class="container">

        <div class="main-menu">
            <div class="logo">
                <a href="<?php echo get_home_url(); ?>"><img
                            src="<?php echo $redux_demo['header-logo']['url']; ?>" alt=""></a>
            </div>

            <h1><?php echo $redux_demo['header-text']; ?></h1>




                <?php

                wp_nav_menu(array(
                    'theme_location'    =>'main',
                    'container'         =>'div',
                    'container_class'   =>'menu',
                    'items_wrap'        =>'<ul>%3$s</ul>',
                    'fallback_cb'       =>'thehammersilver_cb',
                    'walker'            =>new thehammersilver_walker(),



                ));

                ?>

                <!-- <ul>
                    <li><a href="shop.html">Shop</a>
                
                        Megha menu area
                        <ul class="megha-menu">
                            <li>
                                <h4>Jewellery</h4>
                                <a href="categories.html">Ring</a>
                                <a href="categories.html">Pendendan</a>
                                <a href="categories.html">brackles</a>
                                <a href="categories.html">Erraing</a>
                                <a href="categories.html">Cufflink</a>
                                <a href="categories.html">Glod</a>
                                <a href="categories.html">Chain</a>
                            </li>
                            <li>
                                <h4>Accessories</h4>
                                <a href="#">Patches & rings</a>
                                <a href="#">Bol and Ties</a>
                                <a href="#">Motorcycle</a>
                                <a href="#">Home</a>
                                <a href="#">Gift</a>
                            </li>
                            <li>
                                <h4>Apparel</h4>
                                <a href="#">T-Shirt & Hoodies</a>
                                <a href="#">Bags belts & Wallet</a>
                            </li>
                
                
                        </ul>
                
                    </li>
                    <li><a href="#">Gallery</a>
                
                
                        Megha menu
                
                        <ul class="megha-menu">
                            <li>
                                <h4>Archive</h4>
                                <a href="#">Look Book</a>
                                <a href="#">Image</a>
                                <a href="#">Video</a>
                
                            </li>
                            <li>
                                <h4>Instragram</h4>
                                <a href="#">London</a>
                                <a href="#">Lose angles</a>
                                <a href="#">New Yerk</a>
                
                            </li>
                
                        </ul>
                
                
                    </li>
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="music.html">Music</a></li>
                    <li><a href="heritage.html">Heritage</a></li>
                    <li><a href="stories.html">Stores</a></li>
                </ul> -->


        </div>

    </div>
</div>


<!--Main Menu area End-->


