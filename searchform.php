<div class="search-area">
    <form role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
        <input type="text" name="s" placeholder="Search the blog">

        <i class="fa fa-search"></i>
    </form>
</div>